const { BIGINT, STRING, ARRAY } = require('sequelize');

const db = require('../config/db');

const Block = db.define('block', {
  id: {
    type: STRING,
    primaryKey: true,
  },

  votes: STRING,
  difficulty: BIGINT,
  timestamp: BIGINT,
  nonce: BIGINT,
  state_root: STRING,
  n_bits: BIGINT,
  height: BIGINT,
  parent_id: STRING,
  transactions_root: STRING,
  ad_proofs_root: STRING,
  equihash_solutions: ARRAY(BIGINT),
});

module.exports = Block;
