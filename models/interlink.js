const { BIGINT, STRING } = require('sequelize');

const db = require('../config/db');

const Interlink = db.define('interlink', {
  self_id: STRING,
  block_id: STRING,
  level: BIGINT,
});

module.exports = Interlink;
