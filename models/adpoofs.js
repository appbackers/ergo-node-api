const { TEXT, STRING } = require('sequelize');

const db = require('../config/db');

const Adpoof = db.define('adpoff', {
  header_id: STRING,
  proof_bytes: TEXT,
  digest: STRING,
});

module.exports = Adpoof;
