const { FLOAT, STRING } = require('sequelize');

const db = require('../config/db');

const Transaction = db.define('transaction', {
  transaction_id: STRING,
  address_id: STRING,
  block_id: STRING,
  value: FLOAT,
});

module.exports = Transaction;
