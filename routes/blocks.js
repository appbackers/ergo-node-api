const express = require('express');
const router  = express.Router();
const db      = require('../config/db');
const util = require('util');

const Block       = require('../models/block');
const Interlink   = require('../models/interlink');
const Transaction = require('../models/transaction');

router.get('/', function (req, res, next) {
  let { limit = 20, offset } = req.query;

  return db.query('SELECT blocks.*,' +
    ' COUNT(DISTINCT transactions.transaction_id) as "transactionsCount" ' +
    ' ,SUM(transactions.value) as "transactionsSum" ' +
    ' FROM blocks LEFT JOIN transactions ON blocks.id = transactions.block_id ' +
    'GROUP BY blocks.id ORDER BY blocks.timestamp DESC LIMIT ? OFFSET ?', {
    replacements: [limit, offset],
  })
    .then(([data]) => {
      res.json(data);
    });
});

router.get('/search', (req, res) => {
  let { query } = req.query;

  util.log(query);

  let promises = [];

  promises.push(Transaction.findAll({
      where: {
        [db.Op.or]: {
          transaction_id: {
            [db.Op.like]: `${query}%`,
          },
          address_id: {
            [db.Op.like]: `${query}%`,
          },
        },
      },
    }),
  );

  promises.push(Block.findAll({
      where: {
        [db.Op.or]: {
          id: {
            [db.Op.like]: `${query}%`,
          },
          votes: {
            [db.Op.like]: `${query}%`,
          },
        },
      },
    }),
  );

  return Promise.all(promises).then(([transactions, blocks]) => {
    util.log(blocks);

    let blockIds = transactions.map(item => item.block_id);

    util.log(blockIds);

    blockIds = [...blockIds, ...blocks.map(item => item.id)];


    if (blockIds.length === 0) {
      return res.json([]);
    }


    return db.query('SELECT blocks.*,' +
      ' COUNT(DISTINCT transactions.transaction_id) as "transactionsCount" ' +
      ' ,SUM(transactions.value) as "transactionsSum" ' +
      ' FROM blocks LEFT JOIN transactions ON blocks.id = transactions.block_id ' +
      ' WHERE blocks.id IN (?) ' +
      'GROUP BY blocks.id ORDER BY blocks.timestamp DESC LIMIT 10', {
      replacements: [blockIds],
    }).then(([data]) => {
      return res.json(data);
    });
  });

});

router.get('/:id', function (req, res, next) {
  let { id } = req.params;

  return Block.find({
    where: {
      id,
    },
  })
    .then((data) => {
      Block.find({
        where: {
          height: parseInt(data.height) + 1,
        }
      }).then((relatedBlock) => {


        let newData =data.dataValues;
        newData.next_id = relatedBlock.id;

        res.json(newData);
      }).catch(() => {
        res.json(data);
      });

    });
});


const getInterlinkAt = (hash, blockid, level) => {
  return Interlink.find({
    where: {
      block_id: blockid,
      level: level,
    }
  }).then((data) => {
    let newData =  data.dataValues;

    return getInterlinkAt(hash, newData.self_id, newData.level);
  });
};

router.get('/:id/interlinks', function (req, res, next) {
  let { id } = req.params;

  return Interlink.findAll({
    where: {
      block_id: id,
    },
    order: [
      ['level', 'ASC'],
    ],
  })
    .then((data) => {
      res.json(data);
    });
});

router.get('/chart/blocks', (req,res) => {
  return db.query('select TO_CHAR(TO_TIMESTAMP(timestamp / 1000), \'DD/MM/YYYY\') as date, count(*) as value from blocks group by date ORDER BY date DESC;')
    .then(([data])=> {
      res.json(data.map((item) => {
        return [item.date, item.value];
      }));
    })
});

router.get('/:id/transactions', function (req, res, next) {
  let { id } = req.params;

  return Transaction.findAll({
    where: {
      block_id: id,
    },
  })
    .then((data) => {
      res.json(data);
    });
});

module.exports = router;
