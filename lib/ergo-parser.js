const fetch = require('node-fetch');
const db    = require('../config/db');
const util  = require('util');

const Block       = require('../models/block');
const Transaction = require('../models/transaction');
const Interlink   = require('../models/interlink');
const adPoof      = require('../models/adpoofs');

const url = 'http://139.59.254.126:9051';

let offset    = parseInt(process.env.START_OFFSET, 0) || null;
let maxOffset = parseInt(process.env.END_OFFSET, 0) || 999999999999;

function retryFetch (url) {
  return new Promise((resolve) => {
    function doFetch () {
      fetch(url)
        .then((data) => {
          resolve(data);
        })
        .catch((e) => {
          util.log(e);

          util.log('Retrying... ' + url);

          setTimeout(() => {
            doFetch();
          }, 500);
        })
    }

    doFetch();
  });
}

db.sync().then(() => {
  if (offset === null) {
    offset = 0;
  }

  if (offset < 0) {
    offset = 0;
  }

  getBlocks(offset).then(() => {
    util.log('finished');
  });
});

function getBlock (id) {
  let blockUrl = `${url}/blocks/${id}`;

  return retryFetch(blockUrl).then((data) => {
    return data.json();
  })
    .then((data) => {
      return Block.findById(data.header.id).then((data) => {
        if (data) {
          return Interlink.destroy({
            where: {
              block_id: data.id,
            },
          }).then(() => {
            return Transaction.destroy({
              where: {
                block_id: data.id,
              },
            }).then(() => {
              adPoof.destroy({
                where: {
                  header_id: data.id,
                },
              });
            }).then(() => {
              return Block.destroy({
                where: {
                  id: data.id,
                },
              });
            });
          })
        } else {
          return new Promise(resolve => {
            return resolve();
          })
        }
      }).then(() => {
        return Block.create({
          id: data.header.id,
          difficulty: parseInt(data.header.difficulty, 0),
          timestamp: data.header.timestamp,
          height: data.header.height,
          parent_id: data.header.parentId,
          votes: data.header.votes,
          n_bits: data.header.nBits,
          nonce: data.header.nonce,
          state_root: data.header.stateRoot,
          transactions_root: data.header.transactionsRoot,
          ad_proofs_root: data.header.adProofsRoot,
          equihash_solutions: data.header.equihashSolutions,
        }).then((block) => {
          let promises = [];

          data.blockTransactions.transactions.forEach((transaction) => {
            transaction.inputs.forEach((input) => {
              promises.push(Transaction.create({
                transaction_id: transaction.id,
                address_id: input.id,
                block_id: block.id,
              }).catch((e) => {
                util.log(e);
              }));
            });

            transaction.outputs.forEach((output) => {
              promises.push(Transaction.create({
                transaction_id: transaction.id,
                address_id: output.id,
                value: output.value,
                block_id: block.id,
              }).catch((e) => {
                util.log(e);
              }));
            });

          });

          promises.push(adPoof.create({
            header_id: data.adPoofs.headerId,
            digest: data.adPoofs.digest,
            proof_bytes: data.adPoofs.proofBytes,
          }));

          data.header.interlinks.forEach((interlink, index) => {
            promises.push(Interlink.create({
              self_id: interlink,
              level: index,
              block_id: block.id,
            }).catch((e) => {
              util.log(e);
            }))
          });

          return Promise.all(promises);
        }).catch((e) => {
          util.log(e);
        });
      });

    });
}

function getBlocks (offset = 0) {
  let blocksUrl = `${url}/blocks?offset=${offset}&limit=1`;

  util.debug(blocksUrl);

  return fetch(blocksUrl)
    .then((data) => {
      return data.json();
    })
    .then((data) => {
      if (data.length > 0 && offset < maxOffset) {

        let promises = [];

        data.forEach((item) => {
          promises.push(getBlock(item));
        });

        offset += 1;

        return Promise.all(promises).then(() => {
          return getBlocks(offset);
        });
      }
    });
}
